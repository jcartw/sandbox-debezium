# sandbox-debezium

Spin up debezium test environment (may need to cycle down/up):
- `docker-compose up -d`

## Refs

- https://medium.com/nagoya-foundation/simple-cdc-with-debezium-kafka-a27b28d8c3b8
- https://github.com/tramontini/debezium_kafka_medium
- https://hub.docker.com/r/debezium/connect
- https://medium.com/event-driven-utopia/a-visual-introduction-to-debezium-32563e23c6b8
- https://medium.com/event-driven-utopia/a-gentle-introduction-to-event-driven-change-data-capture-683297625f9b
- https://medium.com/event-driven-utopia/8-practical-use-cases-of-change-data-capture-8f059da4c3b7
- https://docs.aws.amazon.com/msk/latest/developerguide/mkc-debeziumsource-connector-example.html
- https://docs.aws.amazon.com/msk/latest/developerguide/aws-access.html
- https://medium.com/event-driven-utopia/8-practical-use-cases-of-change-data-capture-8f059da4c3b7

- https://debezium.io/blog/2019/02/19/reliable-microservices-data-exchange-with-the-outbox-pattern/
- https://debezium.io/documentation/reference/0.9/configuration/outbox-event-router.html
- https://github.com/debezium/debezium-examples/tree/main/outbox

- https://medium.com/yotpoengineering/outbox-with-debezium-and-kafka-the-hidden-challenges-998c00487ae4
- https://medium.com/@huseyinygl/outbox-pattern-implementation-using-debezium-and-google-protobuffers-58b2bd80cc6c
- https://debezium.io/documentation/reference/stable/transformations/outbox-event-router.html#options-for-applying-the-transformation-selectively
- https://debezium.io/documentation/reference/stable/transformations/applying-transformations-selectively.html#applying-transformations-selectively

## Kafka Commands

- list topics: `kafka-topics --bootstrap-server=localhost:9092 --list`
- run consumer: `kafka-console-consumer --bootstrap-server localhost:9092 --topic dbserver1.inventory.customers --from-beginning`
- run consumer: `kafka-console-consumer --bootstrap-server localhost:9092 --topic outbox.event.project --from-beginning`

## Outbox MySQL Schema

```
DROP TABLE IF EXISTS `outbox`;
CREATE TABLE `outbox` (
  `id`             BINARY(36) NOT NULL UNIQUE,
  `aggregatetype`  VARCHAR(255) NOT NULL DEFAULT "NULL",
  `aggregateid`    VARCHAR(255) NOT NULL DEFAULT "NULL",
  `type`           VARCHAR(255) NOT NULL DEFAULT "NULL",
  `payload`        JSON,
  PRIMARY KEY id_pk (`id`)
) ENGINE=InnoDB CHARSET=utf8;
```


```
INSERT INTO outbox SET id=UUID(), aggregatetype='project', aggregateid=UUID(), type='PROJECT_CREATED', payload='{"id": 1, "name": "Test Project Create Event"}';
DELETE FROM outbox;
```
